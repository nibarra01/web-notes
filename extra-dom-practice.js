"use strict";

console.log("*** Task 1 ***");
console.log(document.getElementById("container"));
console.log("");

console.log("*** Task 2 ***");
console.log(document.querySelector("#container"));
console.log("");

console.log("*** Task 3 ***")

// console.log(document.getElementsByClassName("header"));
// console.log(document.getElementsByClassName("header").length);
// console.log(document.getElementsByClassName("header").item(0));
// console.log(document.getElementsByClassName("header").item(0).innerHTML);
document.getElementsByClassName("header").item(0).textContent = "Hello World";
// console.log(document.getElementsByClassName("header").item(0).textContent);

console.log("*** Task 4 ***");

// console.log(document.getElementsByClassName("second"));
var task4var = document.getElementsByClassName("second");
for (var i = 0; i < task4var.length; i++ ){
    console.log(task4var.item(i));
}

console.log("");
console.log("*** Task 5 ***");
for (var i = 0; i < task4var.length; i++ ){
    task4var.item(i).setAttribute("style","font-family : ")
}