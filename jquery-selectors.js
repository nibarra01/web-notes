"use strict";

//id selectors

// var jqueryAlert = $("#name").html();
// alert(jqueryAlert);

// var jqueryAlert = $("#hometown").html();
// alert(jqueryAlert);

// var jqueryAlert = $("#li4").html();
// alert(jqueryAlert);

$("#li4").attr('id', 'li3');
console.log($("#li3").html());
console.log($("#li4").html());

//class selectors
// $('.codebound').css('border', 'red solid 2px');

// element selectors
// $("li").css('font-size', '24px');
//
// $("h1").css(
//     'color', 'red'
// );
//
// $("p").css(
//     'color', 'blue'
// );
//
// $("li").css(
//     'color', 'green'
// );
//
// var alerts = $("h1").html();
// alert(alerts);

// multiple selectors
$('h1, p, li').css(
    {
        'color' : '#bfb9a6',
        'font-size' : '32px',
        'background-image' : 'linear-gradient(to top, #32a852 0%, #3271a8 100%)'
    }
)

console.log($('h1').length);

// for (var n = 0; n < $('h1').length; n++){
//     $('h1').css('color','red');
// }

