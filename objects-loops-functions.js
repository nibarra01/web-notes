"use strict";

// +++++++++++++++++++++++++++++++++=
const albums = [
    {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
];
//+++++++++++++++++++++++++++++++++++++++
var pop = [];
var rock = [];
var pre2000 = [];
var from1990to2020 = [];
var jackson5 = [];

// albums.forEach(function (n){
//     if (n.genre === "Pop"){
//         pop.push(n);
//     }
// })

for (var n = 0; n < albums.length; n++) {
    if (albums[n].genre === "Pop") {
        pop.push(albums[n]);
    }
}

albums.forEach(function (n){
    if (n.genre === "Rock"){
        rock.push(n);
    }
})

albums.forEach(function (n){
    if (n.released < 2000){
        pre2000.push(n);
    }
})

// albums.forEach(function (n){
//     if (n.released >= 1990 && n.released <= 2020){
//         from1990to2020.push(n);
//     }
// })

// for (var i = 0; i < albums.length; i++){
//     if (albums[0].released >= 1990 && albums[0].released <= 2020){
//        console.log(albums[0]);
//     }
// }
//
for (var a2 = 0; a2 < albums.length; a2++) {
    if (albums[a2].released >= 1990 && albums[a2].released <= 2020) {
        from1990to2020.push(albums[a2]);
    }
}

// albums.forEach(function (n){
//     if (n.artist === "Michael Jackson"){
//         jackson5.push(n);
//     }
// })

for (var i = 0; i < albums.length; i++){
    if(albums[i].artist === "Michael Jackson"){
        // console.log(albums[i].title);
        jackson5.push(albums[i]);
    }
}

console.log("Here are all the albums that are Pop: ");
console.log(pop.forEach(function (x){
    console.log(x);
}));

console.log(pop);

console.log(`Here are all the albums that are Rock: `);
console.log(rock.forEach(function (x){
    console.log(x);
}));
console.log(`Here are all the albums that are from 1999 or earlier `);
console.log(pre2000.forEach(function (x){
    console.log(x);
}));
console.log(`Here are all the albums that are from  1990 to 2020`);
console.log(from1990to2020.forEach(function (x){
    console.log(x);
}));
console.log(`Here are all the albums from Michael Jackson: `);
console.log(jackson5.forEach(function (x){
    console.log(x);
}));

// function addAlbum(){
//     var start = confirm("Do you want to add an album?");
//     if (start === true){
//         alert("Adding a new album.");
//         var newArtist = prompt("Artist: ");
//         var newTitle = prompt("Album Title:");
//         var newReleasedate = parseInt(prompt("Album Release Year: "));
//         var newGenre = prompt("Album Genre: ");
//
//         var newAlbum = {
//             artist: newArtist,
//             title: newTitle,
//             released: newReleasedate,
//             genre: newGenre,
//         }
//         albums.push(newAlbum);
//         console.log("added " + newAlbum.title + " to the array");
//         console.log(albums);
//     }
//     else {
//         console.log("Action cancelled by user.")
//     }
// }



var start = confirm("Do you want to add an album?");
if (start === true){
    if (start === true){
        alert("Adding a new album.");
        var newArtist = prompt("Artist: ");
        var newTitle = prompt("Album Title:");
        var newReleasedate = parseInt(prompt("Album Release Year: "));
        var newGenre = prompt("Album Genre: ");
    }
    else {
        console.log("Action cancelled by user.");
    }
}


function addAlbum(artist, title, released, genre) {
    if (start === true){
        var newAlbum = {
            artist: artist,
            title: title,
            released: released,
            genre: genre,
        };
        albums.push(newAlbum);
        console.log("added " + newAlbum.title + " to the array");
        console.log(albums);
    }
}

addAlbum(newArtist, newTitle, newReleasedate, newGenre);

//----------------------------------------
console.log("");
console.log("***Bonus Task 1***");

function fizzbuzz2() {
    for (var i = 1; i <= 100; i++) {
        if (i % 3 === 0 && i % 5 !== 0) {
            console.log("Fizz");
        } else if (i % 3 !== 0 && i % 5 === 0) {
            console.log("Buzz");
        } else if (i % 3 === 0 && i % 5 === 0) {
            console.log("FizzBuzz");
        } else {
            console.log(i);
        }
    }
}

// fizzbuzz2();
console.log("");

//----------------------------------
console.log("");
console.log("***Bonus Task 2***");

function inputTimes5(){
    var one = prompt("input parameter one");
    var two = prompt("input parameter two");

    var one1 = parseInt(one);
    var two2 = parseInt(two);

    while (isNaN(one1)===true){
        alert(`${one} is not a number`);
        one = prompt("input parameter one");
        one1 =parseInt(one);
    }
    while (isNaN(two2)===true){
        alert(`${two} is not a number`);
        two = prompt("input parameter one");
        two2 =parseInt(two);
    }

    var five5 = (one1 + two2) * 5;
    alert(`the sum of ${one1} and ${two2} is ${one1 + two2}\nand that number times 5 is ${five5}`);
}

// inputTimes5();