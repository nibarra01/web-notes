'use strict';

//h1

$('#header1').click(function (){
    var style = document.getElementById('header1').style.backgroundColor;
    if(style !== 'red'){
        $(this).css('background-color', 'red');
    } else {
        $(this).css('background-color', 'white');
    }
});

$('#header2').click(function () {
    var style = document.getElementById('header2').style.backgroundColor;
    if (style !== 'red') {
        $(this).css('background-color', 'blue');
    } else {
        $(this).css('background-color', 'white');
    }
});

$('#header3').click(function () {
    var style = document.getElementById('header3').style.backgroundColor;
    if (style !== 'red') {
        $(this).css('background-color', 'green');
    } else {
        $(this).css('background-color', 'white');
    }
});

//p

$('#alpha').dblclick(function (){
    var style = document.getElementById('alpha').style.fontSize;
    if (style !== '18px'){
        $(this).css("font-size", "18px");
    }else {
        $(this).css("font-size", '12px');
    }
});

$('#beta').dblclick(function (){
    var style = document.getElementById('beta').style.fontSize;
    if (style !== '18px'){
        $(this).css("font-size", "18px");
    }else {
        $(this).css("font-size", '12px');
    }
});

$('#gamma').dblclick(function (){
    var style = document.getElementById('gamma').style.fontSize;
    if (style !== '18px'){
        $(this).css("font-size", "18px");
    }else {
        $(this).css("font-size", '12px');
    }
});

$('#delta').dblclick(function (){
    var style = document.getElementById('delta').style.fontSize;
    if (style !== '18px'){
        $(this).css("font-size", "18px");
    }else {
        $(this).css("font-size", '12px');
    }
});

// li
$('li').hover(
    function (){
      $(this).css('color', 'green');
    },
    function (){
        $(this).css('color', 'black');
    }
);

//keydown/keyup
// $(document).keyup(
//     function (){
//         console.log("Keyup event triggered!");
//     }
// );

// $(document).keydown(
//     function (){
//         alert("Keydown event triggered!");
//     }
// );


//form
var n = 0
$('#text-form').keydown(
    function (){
        n++;
        $('#counter').html(`keypress #${n}`);
    }
);