"use strict";

const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];


// -----------------------------------------------------------------------
// const fiveLanguages = developers.filter(function (l){
//     if (l.languages.length >= 5){
//         return l
//     }
// });
//
// const fiveLanguagesES6 = developers.filter(l => l.languages.length >= 5);
// console.log(fiveLanguagesES6);
//-----------------------------------------------------------------------------


//--------------------------------------------------------

// let emails = [];
//
// while (emails.length < developers.length){
//     emails.push('');
// }

// const emails = developers.map(e => e.email)
// console.log(emails);

// -------------------------------------------------------------

// --------------------------------------------------------

// const years = developers.reduce((a,b) => {
//     return a + b.yearExperience;
// }, 0);
// console.log(`years total: ${years}`);
// const average = years / developers.length
// console.log(`average years: ${average}`);

// -----------------------------------------------------


// -------------------------------------------------
// let e = ''
// const longmail = developers.reduce((a, b) => {
//     let c = developers.length - 1;
//     let d = '';
//     for (c; c >= 0; c--){
//         // console.log(c);
//         if (developers[c].email.length < b.email.length){
//             console.log("---")
//             console.log(developers[c].email.length)
//             console.log(developers[c].email)
//             console.log(b.email.length)
//             console.log(b.email)
//             console.log("---")
//
//             d = b.email;
//         }
//
//         if (d.length > e.length){
//             d = e;
//         }
//         console.log("***")
//         console.log(d);
//         console.log(b.email);
//         console.log(e)
//         console.log("***")
//     }
//
//     console.log("====")
//     console.log(a)
//     console.log(b.email)
//     console.log(d)
//     console.log(e)
//     console.log("====")
//
// },'')
// console.log(longmail);

// const longestEmail = developers.reduce((a,b) => {
//     return a.email.length > b.email.length ? a : b;
// }).email
//
// console.log(longestEmail);
//
// const shortestEmail = developers.reduce((c,d) => {
//     return c.email.length < d.email.length ? c : d;
// }).email
//
// console.log(shortestEmail);
//----------------------------------------

// const developerNames = developers.reduce((a,b) => {
//
//     return a += b.name + ', ';
// },'CodeBound staff: ');
// console.log(developerNames)


// **************************************************************************************

const albums = [
    {artist: 'Michael Jackson', title: 'Thriller', released: 1982, genre: 'Pop'},
    {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}];

const pop = albums.filter(a => a.genre === 'Pop')
console.log(pop);

const rock = albums.filter(a => a.genre === 'Rock')
console.log(rock);

const albumyears = albums.reduce((x,n) =>{
    return x + n.released
},0 )

console.log(albumyears)





// *******************************************************************

const companies = [
    {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];

let array = [];
const companyNames = companies.map(function (name){
    // console.log(name.name)
    return name.name
});
console.log(companyNames);

const companies1989 = companies.filter(year => year.start_year < 1990);
console.log(companies1989);

const retail = companies.filter(a => a.category === 'Retail');
console.log(retail);

const techFinan = companies.filter(a => a.category === 'Technology' || a.category === 'Financial');
console.log(techFinan);

const companyNY = companies.filter(a => a.location.state === 'NY');
console.log(companyNY);

const evenYear = companies.filter(a => a.start_year % 2 === 0)
console.log(evenYear);

const yearTimesTwo = companies.map( a => a.start_year * 2)
console.log(yearTimesTwo);

const yearReduce = companies.reduce((total, year) => {
    return total + year.start_year
},0);
console.log(yearReduce);


const companyNamesALP = companies.map(alp => alp.name);
companyNamesALP.sort();
console.log(companyNamesALP);

const sortedCompanies = companies.sort(function (c1,c2){
    if (c1.start_year < c2.start_year){
        return 1
    } else {
        return -1
    }
});
console.log(sortedCompanies);

const filter8 = companies.map(x => x.start_year);
filter8.sort();
const filter9 = filter8.reverse();
console.log(filter9);