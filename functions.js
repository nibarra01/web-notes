//Functions

// -----------------------------------------------------------------------------------

function  count(input){
    console.log(input.length);
}

function add(a,b){
    console.log(a+b);
}

function subtract(a,b){
    console.log(a-b);
}

function divide(numerator,denominator){
    console.log(numerator/denominator);
}

function remainder(number,divisor){
    console.log(number%divisor);
}

function square(a){
    console.log(a*a);
}

//anonymous functions variables
var input = prompt("input string", "Hello World");
var a = prompt("input variable 'a' for addition a + b", "2");
var b = prompt("input variable 'b' for addition a + b", "2");
var c = prompt("input variable 'c' for subtraction c - d", "2");
var d = prompt("input variable 'd' for subtraction c - d", "2");
var e = prompt("input variable 'e' for division e / f", "2");
var f = prompt("input variable 'f' for division e / f", "2");
var g = prompt("input variable 'g' for division g / h output: remainder", "2");
var h = prompt("input variable 'h' for division g / h output: remainder", "2");
var i = prompt("input variable 'i' for multiplication i^2", "2");


// convert inputs from strings to numbers
var numA = parseInt(a);
var numB = parseInt(b);
var numC = parseInt(c);
var numD = parseInt(d);
var numE = parseInt(e);
var numF = parseInt(f);
var numG = parseInt(g);
var numH = parseInt(h);
var numI = parseInt(i);



//what i tried before googling again
// if (numA == NaN) {
// if (typeof numA === string {
//while (typeof numB === string) {



// force user to input numbers
while (isNaN(numA)) {
    var a = prompt("use a number", "4");
    var numA = parseInt(a);
}

while (isNaN(numB)) {
    var b = prompt("use a number", "4");
    var numB = parseInt(b);
}

while (isNaN(numC)) {
    var c = prompt("use a number", "4");
    var numC = parseInt(c);
}

while (isNaN(numD)) {
    var d = prompt("use a number", "4");
    var numD = parseInt(d);
}

while (isNaN(numE)) {
    var e = prompt("use a number", "4");
    var numE = parseInt(e);
}

while (isNaN(numF)) {
    var f = prompt("use a number", "4");
    var numF = parseInt(f);
}

while (isNaN(numG)) {
    var g = prompt("use a number", "4");
    var numG = parseInt(g);
}

while (isNaN(numH)) {
    var h = prompt("use a number", "4");
    var numH = parseInt(h);
}

while (isNaN(numI)) {
    var i = prompt("use a number", "4");
    var numI = parseInt(i);
}



// Anonymous function 1
console.log(`display string length`);
(function () {
    console.log(input.length);
})();
// display variable
console.log(`string was ${input}`);
console.log(` `);

// Anonymous function 2
console.log(`addition: `);
(function () {
    console.log(numA + numB);
})();

// display variables
console.log(`variable a was ${numA}`);
console.log(`variable b was ${numB}`);
console.log(` `);

// Anonymous function 3
console.log(`subtraction: `);
(function () {
    console.log(c-d);
})();

// display variables
console.log(`variable c was ${c}`);
console.log(`variable d was ${d}`);
console.log(` `);

// Anonymous function 4
console.log(`division: `);
(function () {
    console.log(e/f);
})();

// display variables
console.log(`variable e was ${e}`);
console.log(`variable f was ${f}`);
console.log(` `);

// Anonymous function 5
console.log(`modulus: `);
(function () {
    console.log(g%h);
})();

// display variables
console.log(`variable g was ${g}`);
console.log(`variable h was ${h}`);
console.log(` `);

// Anonymous function 6
console.log(`square: `);
(function () {
    console.log(i*i);
})();

// display variables
console.log(`variable i was ${i}`);
console.log(` `);


// ----------------------------------------------------------------------------



function isOdd(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else {
        return x % 2 != 0;
    }
}

// console.log(isOdd(7));

function isEven(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else {
        return x % 2 == 0;
    }
}

// console.log(isEven(8));


function isSame(input){
// I don't understand the question


    var num1 = "10";
    return num1 === input;
}

function isSeven(x){
    return x == 7;
}
// console.log(isSeven(7));


function addTwo(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else{
        return numx + 2;
    }
}
// console.log(addTwo(16));


function isMultipleOfTen(x) {
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else {
        return x % 10 === 0;
    }
}
// console.log(isMultipleOfTen(20));

function isMultipleOfTwo(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else {
        return x % 2 === 0;
    }

}

// console.log(isMultipleOfTwo(17));



function isMultipleOftwoAndFour(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else {
        return x % 4 === 0;
    }

}
// console.log(isMultipleOftwoAndFour(8));

function isTrue(input){
    return input === "true";
}
// console.log((isTrue("true")));

function isFalse(input){
    return input === "false";
}
// console.log(isFalse("false"));

function isVowel(input){
    switch (input) {
        case"a":
            return true;
        case"e":
            return true;
        case"i":
            return true;
        case"o":
            return true;
        case"u":
            return true;
        case"A":
            return true;
        case"E":
            return true;
        case"I":
            return true;
        case"O":
            return true;
        case"U":
            return true;
        default:
            return false;
    }
}
// console.log(isVowel("i"));

function triple(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else{
        return numx * 3;
    }
}
// console.log(triple(9));

function  QuadNum(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else{
        return numx * 4;
    }
}

// console.log(QuadNum(8));

function modulus(a,b){
    var numa = parseInt(a);
    var numb = parseInt(b);
    if(isNaN(numa) && isNaN(numb)){
        return "neither variable is a number";
    }
    if(isNaN(numa)) {
        return "variable a is Not a number";
    }
    if(isNaN(numb)) {
        return "variable b is Not a number";
    }
    return a % b;
}
// console.log(modulus(17,5));


function degreesToRadians(x){
    var numx = parseInt(x);
    if(isNaN(numx)) {
        return "Not a number";
    }
    else{
        return numx * Math.PI / 180;
    }
}
// console.log(degreesToRadians(90));


function absoluteValue(x){
    var numx = parseInt(x);
    var zero = "Number is zero"

    while (isNaN(numx)===true){
            var while1 = prompt("Not a number.","50");
            numx = parseInt(while1);
        }

    if (numx === 0){
        return zero;
    }else if (numx < 0){
        return numx * -1;
    }
    else {
        return numx;
    }

}
var n = prompt("Insert number for absolute value")
console.log(absoluteValue(n));


var reverse = prompt("input string for reversal")
function reverseString(x){
    // Jose
    var splitString = x.split(""); // 'H', 'e', 'l', 'l', 'o'
    var reverseArray = splitString.reverse(); // 'o', 'l', 'l', 'e', 'H'
    var joinArray = reverseArray.join(""); // elloH
    // return joinArray;
    // It's so simple I'm not sure I would do it any different other than naming the variables different and stylizing the output a bit
    return `String entered was: "${x}". The reversed string is: "${joinArray}"`;
}
reverseString(reverse);