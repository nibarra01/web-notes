"use strict";


//---------------------------------
console.log("*** task 1 ***")
console.log("step 1");
var sports = [];
sports.push("basketball", "baseball", "football", "hockey", "golf");
console.log(sports);
console.log("");

console.log("step 2");
sports.sort();
console.log(sports);
console.log("");

console.log("step 3");
sports.reverse();
console.log(sports);
console.log("");

console.log("step 4");
sports.push("tennis", "soccer", "rugby");
console.log(sports);
console.log("");

console.log("step 5");
console.log(sports.indexOf("basketball"));
console.log(sports[3]);
console.log("");

console.log("step 6");
var half1 = sports.slice(0,2);
var half2 = sports.slice(4,7);
console.log(half1);
console.log(half2);
sports = [];
half1.forEach(function (x){
    sports.push(x);
})
half2.forEach(function (x){
    sports.push(x);
})
console.log(sports);
console.log("");

console.log("step 7");
sports.shift();
console.log(sports);
console.log("");

console.log("step 8");
sports = sports.join(", ");
console.log(sports);
console.log("");

//----------------------------
console.log("*** task 2 ***");
console.log("step 1")
const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
console.log(bands[2]);
console.log("");

console.log("step 2");
bands.unshift("Fleetwood Mac");
console.log(bands);
console.log("");

console.log("step 3");
bands.push("Journey");
console.log(bands);
console.log("");

//---------------------------------

console.log("*** task 3 ***");
console.log("step 1")
var charlie = ['josh', 'jose', 'nick'];
console.log(charlie[0]);
console.log(charlie[1]);
console.log(charlie[2]);
console.log("");

console.log("step 2");
for (var x = 0; x < charlie.length; x++){
    console.log(charlie[x]);
}
console.log("");

console.log("step 3");
charlie.forEach(function (x){
    console.log(x);
})
console.log("");


//-------------------------------------

console.log("*** task 4 ***");
var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];

var n = 0;
for (var x = 0; x < arrays1.length; x++ ){
    if (arrays1[x] == "CodeBound"){
        delete arrays1[x];
        n++;
    }
}
arrays1.sort();

while (n > 0){
    arrays1.pop();
    n--;
}
console.log(arrays1);

console.log("");

//-----------------------------

console.log("*** task 5 ***");
var numbersTask5 = [3, '12', 55, 9, '0', 99];

var num0 = numbersTask5[0];
var num1 = numbersTask5[1];
var num2 = numbersTask5[2];
var num3 = numbersTask5[3];
var num4 = numbersTask5[4];
var num5 = numbersTask5[5];

num0 = parseInt(num0);
num1 = parseInt(num1);
num2 = parseInt(num2);
num3 = parseInt(num3);
num4 = parseInt(num4);
num5 = parseInt(num5);

console.log(`${num0} + ${num1} + ${num2} + ${num3} + ${num4} + ${num5} = ${num0+num1+num2+num3+num4+num5}`);
console.log("");

/*
var sum = 0
numbers.forEach(function (number){
    sum += parseInt(number);
    console.log(sum);
 */

//----------------------------------

console.log("*** Task 6 ***");
var numbersTask6 = [7, 10, 22, 87, 30];

var num6 = numbersTask6[0];
var num7 = numbersTask6[1];
var num8 = numbersTask6[2];
var num9 = numbersTask6[3];
var num10 = numbersTask6[4];

numbersTask6.forEach(function (x){
    console.log(`old number: ${x}, new number: ${parseInt(x) * 5}`);
})

numbersTask6[0] = num6 * 5;
numbersTask6[1] = num7 * 5;
numbersTask6[2] = num8 * 5;
numbersTask6[3] = num9 * 5;
numbersTask6[4] = num10 * 5;

console.log(numbersTask6)
console.log("");

//------------------------------------

console.log("*** Task 7 ***");
function removeFirstLast() {
    var input = prompt("Input a string");
    var string = input.split("");

    console.log(`removing first and last characters from '${input}'`)

    string.shift();
    string.pop();
    string = string.join("");

    console.log(`end result: ${string}`);
}


removeFirstLast();

console.log("");

//----------------------------------------------

console.log("*** Task B-1 ***");
function phoneNum(){
    var input = prompt("insert phone number");
    input.split(" ");
    console.log(input);
    input.split("-");
    console.log(input);
    input.split("(");
    console.log(input);
    input.split("(");
    console.log(input);
    var input2 = parseInt(input);
    var input3 = input2;
    toString(input3);

    while (isNaN(input2)===true){
        input = prompt("invalid input. Input phone number");
        console.log("checkpoint 1");
        input.split(" ");
        input.split("-");
        input.split("(");
        input.split("(");
        input2 = parseInt(input);
        input3 = input2
        toString(input3);
    }

    console.log("checkpoint 2");

    while (input3.length !== 10){
        input = prompt("Length needs to be 10");
        input2 = parseInt(input);
        input3 = input2;
        toString(input3);
        console.log("checkpoint 3");
            while (isNaN(input2)===true){
                    console.log("checkpoint 4")
                    input = prompt("invalid input. Input phone number");
                    input.split(" ");
                    input.split("-");
                    input.split("(");
                    input.split("(");
                    input2 = parseInt(input);
                    input3 = input2;
                    toString(input3);
                }
    }

    console.log("checkpoint final");

}

phoneNum();