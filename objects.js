"use strict";

//--------------------------------------
console.log("*** Task 1 ***");
var movieTask1 = {
    title: "Star Trek VI: The Undiscovered Country",
    genre: ["Action", "Adventure", "SciFi", "Thriller"],
    starring: ["William Shatner", "Leonard Nimoy", "DeForest Kelley"],
    runtime: {
        hours: 1,
        minutes: 50,
    }
}


console.log(movieTask1.title);
console.log(movieTask1.genre);
console.log(movieTask1.starring);
console.log(movieTask1.runtime);
console.log("");

//-------------------------------------------

console.log("*** Task 2 ***");
var movieTask2 = {
    title:"Star Trek II: The Wrath of Kahn",
    protagonist:"Admiral James T. Kirk",
    antagonist:"Khan Noonien Singh",
    genere: ["Action", "Adventure", "SciFi",],
}

console.log(movieTask2.title);
console.log(movieTask2.protagonist);
console.log(movieTask2.antagonist);
console.log(movieTask2.genere);
console.log("");

//-----------------------------------------------

console.log("*** Task 3 ***");
var pet = {
    name: "Santa's Little Helper",
    age: 31,
    species: "Greyhound",
    valueInDollars: 600000000,
    nicknames: ["No. 8", "Suds McDuff", "Santa's Stupid Name"],
    vaccinations: ["March 12, 1992", "gastric torsion", "didn't see any history of vaccinations on the show, this was the closest thing I found."]
}

console.log(pet);
console.log("");

//--------------------------------------------

console.log("*** Task 4 ***");
var movies = [
    {
        title: "Star Trek VI: The Undiscovered Country",
        genre: ["Action", "Adventure", "SciFi", "Thriller"],
        starring: ["William Shatner", "Leonard Nimoy", "DeForest Kelley"],
        runtime: {
            hours: 1,
            minutes: 50,
        },
        releaseDate: {
            day: 6,
            month: 12,
            year: 1991,
        },
        director: {
            firstName: "Nicholas",
            lastName: "Meyer",
        },
        awards: [
            {
                dontKnowWhatThisIsCalled: "Academy of Science Fiction, Fantasy & Horror Films",
                award:"Best Science Fiction Film",
                year: 1993,
                awardedTo: "-",
            },
        ],
        // tootenScore:
        myReview: function (){
            console.log("placeholder text");
        }
    },{
        title: "The Iron Giant",
        genre: ["Animation", "Action", "Adventure"],
        starring: ["Eli Marienthal", "Harry Connick Jr.", "Jennifer Aniston"],
        runtime: {
            hours: 1,
            minutes: 26,
        },
        releaseDate: {
            day: 6,
            month: 8,
            year: 1999,
        },
        director: {
            firstName: "Brad",
            lastName: "Bird",
        },
        awards: [
            {
                dontKnowWhatThisIsCalled: "BAFTA Awards",
                award:"Best Feature Film",
                year: 2000,
                awardedTo: ["Allison Abbate", "Des McAnuff", "Brad Bird", "Tim McCanlies"],
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Character Animation",
                year: 1999,
                awardedTo: "Steve Markowski",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Effects Animation",
                year: 1999,
                awardedTo: "Allen Foster",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Music in an Animated Feature Production",
                year: 1999,
                awardedTo: "Michael Kamen (composer)",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Production Design in an Animated Feature Production",
                year: 1999,
                awardedTo: "Alan Bodner",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Storyboarding in an Animated Feature Production",
                year: 1999,
                awardedTo: "Mark Andrews",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Directing in an Animated Feature Production",
                year: 1999,
                awardedTo: "Brad Bird",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Voice Acting in an Animated Feature Production",
                year: 1999,
                awardedTo: "Eli Marienthal For playing \"Hogarth Huges\"",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Achievement in an Animated Theatrical Feature",
                year: 1999,
                awardedTo: ["Warner Bros.", "Warner Bros. Feature Animation"],
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Individual Achievement for Writing in an Animated Feature Production",
                year: 1999,
                awardedTo: ["Tim McCanlies (screenplay)", "Brad Bird (story)"],
            },
            {
                dontKnowWhatThisIsCalled: "Dallas-Fort Worth Film Critics Association Awards",
                award:"Best Animated Film",
                year: 2000,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Florida Film Critics Circle Awards",
                award:"Best Animation",
                year: 2000,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Genesis Awards",
                award:"Feature Film - Animated",
                year: 2000,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Las Vegas Film Critics Society Awards",
                award:"Best Animated Film",
                year: 2000,
                awardedTo: "Brad Bird",
            },
            {
                dontKnowWhatThisIsCalled: "Los Angeles Film Critics Association Awards",
                award:"Best Animation",
                year: 1999,
                awardedTo: "Brad Bird",
            },
            {
                dontKnowWhatThisIsCalled: "Motion Picture Sound Editors, USA",
                award:"Best Sound Editing - Animated Feature",
                year: 2000,
                awardedTo: ["Dennis Leonard (supervising sound editor)", "Randy Thom (supervising sound editor/sound effects designer)", "Curt Schulkey (supervising dialogue editor)", "Beau Borders (sound editor)", "Mary Helen Leasman (foley editor)", "Andrea Gard (assistant sound editor)", "Yin Cantor (assistant sound editor)", "Joanna Laurent (assistant sound editor)", "Dennie Thorpe (foley artist)", "Jana Vance (foley artist)", "Tony Eckert (foley mixer)", "Troy Porter (adr mixer)", "Doc Kane (original dialogue recording)", "Gregory H. Watkins (re-recording mixer)", "Kevin E. Carpenter (re-recording mixer)", "Frank 'Pepe' Merel (foley recordist)"]
            },
            {
                dontKnowWhatThisIsCalled: "New York Film Critics Circle Awards",
                award:"Best Animated Film",
                year: 1999,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Santa Fe Film Critics Circle Awards",
                award:"Best Animated Film",
                year: 2000,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Satellite Awards",
                award:"Best Youth DVD",
                year: 2005,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Young Artist Awards",
                award:"Best Performance in a Voice-Over (TV or Feature Film) - Young Actor",
                year: 2000,
                awardedTo: "Eli Marienthal",
            },
            {
                dontKnowWhatThisIsCalled: "YoungStar Awards",
                award:"Best Young Voice Over Talent",
                year: 2000,
                awardedTo: "Eli Marienthal",
            },
        ],
        // tootenScore:
        myReview: function (){
            console.log("placeholder text");
        }
    },
    {
        title: "Galaxy Quest",
        genre: ["Adventure", "Comedy", "SciFi"],
        starring: ["Tim Allen", "Sigourney Weaver", "Alan Rickman"],
        runtime: {
            hours: 1,
            minutes: 42,
        },
        releaseDate: {
            day: 25,
            month: 12,
            year: 1999,
        },
        director: {
            firstName: "Dean",
            lastName: "Parisot",
        },
        awards: [
            {
                dontKnowWhatThisIsCalled: "Academy of Science Fiction, Fantasy & Horror Films",
                award:"Best Actor",
                year: 2000,
                awardedTo: "Tim Allen",
            },
            {
                dontKnowWhatThisIsCalled: "Amsterdam Fantastic Film Festival",
                award:"Best Actor",
                year: 2000,
                awardedTo: "Tim Allen",
            },
            {
                dontKnowWhatThisIsCalled: "Brussels International Festival of Fantasy Film (BIFFF)",
                award:"Pegasus Audience Award",
                year: 2000,
                awardedTo: "Dean Parisot",
            },
            {
                dontKnowWhatThisIsCalled: "Brussels International Festival of Fantasy Film (BIFFF)",
                award:"Best Screenplay",
                year: 2000,
                awardedTo: "David Howard",
            },
            {
                dontKnowWhatThisIsCalled: "Hochi Film Awards",
                award:"Best Foreign Language Film",
                year: 2001,
                awardedTo: "Dean Parisot",
            },
            {
                dontKnowWhatThisIsCalled: "Hugo Awards",
                award:"Best Dramatic Presentation",
                year: 2000,
                awardedTo: ["Dean Parisot (director)", "David Howard (story/screeplay)", "Robert Gordon (screeplay)"]
            },
            {
                dontKnowWhatThisIsCalled: "Las Vegas Film Critics Society Awards",
                award:"Best Visual Effects",
                year: 2000,
                awardedTo: "Bill George",
            },
            {
                dontKnowWhatThisIsCalled: "Science Fiction and Fantasy Writers of America",
                award:"Best Script",
                year: 2001,
                awardedTo: ["Robert Gordon", "David Howard"],
            },
        ],
        // tootenScore:
        myReview: function (){
            console.log("placeholder text");
        }
    },{
        title: "The Wind Rises",
        genre: ["Animation", "Biography", "Drama"],
        starring: ["Hideaki Anno", "Hidetoshi Nishijima", "Miori Takimoto"],
        runtime: {
            hours: 2,
            minutes: 6,
        },
        releaseDate: {
            day: 20,
            month: 7,
            year: 2013,
        },
        director: {
            firstName: "Hayao",
            lastName: "Miyazaki",
        },
        awards: [
            {
                dontKnowWhatThisIsCalled: "Alliance of Women Film Journalists",
                award:"Best Animated Feature Film",
                year: 2013,
                awardedTo: "Hayao Miyazaki",
            },
            {
                dontKnowWhatThisIsCalled: "Annie Awards",
                award:"Outstanding Achievement in Writing in an Animated Feature Production",
                year: 2014,
                awardedTo: "Hayao Miyazaki",
            },
            {
                dontKnowWhatThisIsCalled: "Awards of the Japanese Academy",
                award:"Best Animation Film",
                year: 2014,
                awardedTo: "Hayao Miyazaki (director)",
            },
            {
                dontKnowWhatThisIsCalled: "Awards of the Japanese Academy",
                award:"Best Music Score",
                year: 2014,
                awardedTo: "Joe Hisaishi",
            },
            {
                dontKnowWhatThisIsCalled: "Boston Online Film Critics Association",
                award:"Best Animated Film, Tied with Frozen (2013)",
                year: 2013,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Boston Society of Film Critics Awards",
                award:"Best Animated Film",
                year: 2013,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Central Ohio Film Critics Association",
                award:"Best Animated Film",
                year: 2014,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Central Ohio Film Critics Association",
                award:"Best Foreign Language Film",
                year: 2014,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Chicago Film Critics Association Awards",
                award:"Best Animated Feature",
                year: 2013,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Chicago Film Critics Association Awards",
                award:"Best Foreign Language Film",
                year: 2013,
                awardedTo: "-",
            },
            {
                dontKnowWhatThisIsCalled: "Golden Trailer Awards",
                award:"Best Foreign TV Spot",
                year: 2014,
                awardedTo: ["Studio Ghibli", "Walt Disney Studios Motion Pictures", "Trailer Park", "For the TV commercial entitled \"Hayao Miyazaki - Visionary 30\""],
            },
            {
                dontKnowWhatThisIsCalled: "Heartland Film",
                award:"Truly Moving Picture Award",
                year: 2014,
                awardedTo: ["Hayao Miyazaki (director)", "Studio Ghibli (production company)", "Touchstone Pictures (distributor)"],
            },
            {
                dontKnowWhatThisIsCalled: "International Film Music Critics Award (IFMCA)",
                award:"Best Original Score for an Animated Film",
                year: 2014,
                awardedTo: "Joe Hisaishi",
            },
            {
                dontKnowWhatThisIsCalled: "Mill Valley Film Festival",
                award:"Animation",
                year: 2013,
                awardedTo: "Hayao Miyazaki",
            },
        ],
        // tootenScore:
        myReview: function (){
            console.log("placeholder text");
        }
    }
]

console.log(movies.forEach(function (moo){
       console.log(`${moo.title} was released on ${moo.releaseDate.month}/${moo.releaseDate.day}/${moo.releaseDate.year}`);
       console.log(`${moo.title} was directed by ${moo.director.firstName} ${moo.director.lastName}`);
       console.log(`${moo.title} stars: ${moo.starring}`);
       console.log(`${moo.title}'s runtime is ${moo.runtime.hours}h ${moo.runtime.minutes}m`);

       console.log(`${moo.title} recieved the following awards:`);
       console.log(moo.awards.forEach(function (good){
           if(good.awardedTo === "-"){
               console.log(`${good.award} from the ${good.dontKnowWhatThisIsCalled} in ${good.year}`);
           }
           else{
               console.log(`${good.award} from the ${good.dontKnowWhatThisIsCalled} to ${good.awardedTo} in ${good.year}`);
           }
       }
       )
       );
       console.log(" ");
}
)
);
