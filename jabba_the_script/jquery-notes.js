"use strict";

// Document Ready
// window.onload = function (){
//     alert("this page has finished loading")
// };

// jQuery object
/*
* object used to find and create HTML elements from the DOM
*
*
* syntax:
* $(document).ready(function(){
*   code
* });
*
* */

//jQuery document ready
$(document).ready(function (){
    alert("This page has finished loading")
});

// in jQuery we use teh dollar sign to reference the jQuery object
// $ is an alias of jQuery

// Jquery Selectors
// ID, class, element, multiple, all
/*
#
.
elementTagName
*
 */
// syntax for jquery selectors
// $("selector")
// .html - returns the html content(s) of the selected element(s)
// similar to '.innerHTML' property
// .css - allows us to change css properties for the selected element(s)
// similar to the '.style' property

//example
//id selector
var content = $("#codebound").html();
alert(content);


//css selector
$('.urgent').css('background-color', 'red') // single property
/*
same as in css

.urgemt{
background color: 'red';
}
 */

$(".not-urgent").css({"background-color": "yellow", 'text-decoration': 'line-through'}); // multiple properties

//multiple selector
$('.urgent, p').css(
    'color', 'orange'
);

$('.urgent, p').css( 'text-decoration', 'underline' );

//all selector
//$('*')

//element selector
$("h3").css('border-style', 'dotted');