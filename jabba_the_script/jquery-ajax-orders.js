"use strict";
// Basic request
// $.ajax("/some-url");
//defaults to a GET request

//AJAX options
// $.ajax("/some-url", {
//     type: "POST",
//     data: {
//         name: "John",
//         location: "San Antonio"
//     }
// });
//the easiest way to manipulate the ajax options to pass a JavaScript object
/*
Most common options
type - type of http request to send to the server
    "GET", "POST". "PUT", "DELETE"
data - data to be included with the request
dataType - the type of data we expect the server to send back from out request
    "json", "xml", "html", "text"
url - rather than passing the request url as a string, you can pass a javascript object on its own
username & password - if a server requires a username and password you can specify these parameters
headers - an object whose key value pairs represent custom http headers to send along with the request
 */


//handling responses
// $.ajax("/some/path/to-a/file.json").done(function (data, status, jqXhr){
    /// what we want the code to do or want to do with the data being pulled

    // data - the body of the response from the server
    // status - a string indicating the status of the request
    // jqXhr - a jQuery object that represents the ajax request
// })

// more methods for handling responses
/*
.fail()
.always()

 */




var ajaxRequest = $.ajax("data/orders.json");
ajaxRequest.done(function (data){
    // console.log(data);
    var dataHTML = buildHTML(data);
    $('#orders').html(dataHTML);

});

function buildHTML(orders){
    var orderHTML = '';
    orders.forEach(function (order){
        orderHTML += "<section>";
        orderHTML += "<dl>";
        orderHTML += "<dt>" + "Ordered Item" + "</dt>";
        orderHTML += "<dd>" + order.item + "</dd>";
        orderHTML += "<dt>" + "Ordered By" + "</dt>"
        orderHTML += "<dd>" + order.orderedBy + "</dd>"
        orderHTML += "</dl>";
        orderHTML += "</section>";

    });
    return orderHTML;
}