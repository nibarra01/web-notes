// External JavaScript
// single line comment
/*
multiline
comment
 */

// console.log(5+100)

/*      Data types
            primitive types
            numbers, strings, boolean, undefined, null, objects

        Variables
          var, let, const

        Declaring Variables
          syntax: var nameOfTheVariable;
          syntax: var nameOfVariable = value;
          let nameOfVariable = value;
          const nameoOfVariable = value;
*/

//  declared a variable named firstName
var firstName;
//  assigned the value of "Billy" to firstName
firstName = "Billy";
//  called the variable name
console.log(typeof firstName);
// alert(firstName);

// var firstName = "Billy";

/*
    Operators
        Arithmetic
            + addition
            - subtraction
            * multiplication
            / divide
            % modulus
*/

console.log(15 % 4);
console.log(15 / 4);

console.log(
    (1+2) * 4/5
);

/*
    Logical Operators
        && AND
        || OR
        !  NOT
        return boolean value
        also used with non-boolean values

        true && true = true
        true && false = false
        false && false = true
        false && true = true

        true || false = true
        true || true = true
        false || false = false
        false || true = true

        !true = false
        !false = true

        =, ==, ===
        = used to assign value to a variable
        var lastName = "Smith"

        Comparison operator
        == checks if the value is the same
        === checks if the value and the data type are the same
*/
        // var firstPet = "Dog";
        // var secondPet = "Cat";
        // var secondPet = "dog";
        // var secondPet = "Dog";
        // console.log(firstPet === secondPet);

        // var age = 12;
        // var age2 = "12";
        // console.log(age == age2);
        // console.log(age === age2);

                // != checks if the value is not the same
                // !== if the value and data type is not the same

        // console.log(age != age2); // false because age = 12 and age2 = 12
        // console.log(age !== age2); // true because age == number and age2 == string

//     <, >, <=, >=


    const age = 21;
    console.log(age >= 21 || age < 18);

    console.log(age <= 21 || age > 18);

    console.log(age < 21 && age > 18);

    // Concatenation
const person ="Bruce";
const person_age = 40;
console.log("Hello my name is " + person + " and I am " + person_age + " years old.");


//Template strings
console.log(`Hello my name is ${person} and I am ${person_age} years old.`);

//Get the length of a string
console.log(person.length);

const greeting = "Hello World!"
console.log(greeting.length);

//get the string in uppercase/lowercase
console.log(greeting.toUpperCase());
console.log(greeting.toLowerCase());

//substring
console.log(greeting.substring(0,5));

//Functions
// - a reusable block that performs a specified task(s)

// Syntax for function:
/*
* function nameOfTheFunction(){
* code you want the function to do
* }*/

// function sayHello() {
//     alert("Hello!!!");
// }

function sayHello() {
    console.log("Hello!!!")
}

// Calling a function
// when we want it to run, we call it by its name with the ()

sayHello()


function addNumber(num1) {
    console.log(num1+2);
}

addNumber(21);

function message(name,age){
    console.log(`Hello ${name} you are ${age} years old`)
}

message("Clark", 50);