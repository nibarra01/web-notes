"use strict";

// BOM notes
// browser object model
/*
Hierarchy of objects in the browser
we can target and manipulate html elements with javascript

Location object
    - manipulate the location of a document
    - get query string parameter(s)

Navigator object
    - query the info and capabilities of the browser

Screen object
    - gets info and manage the web browser's screen

History object
    - get info/manage web browser's history

Window
Document Object Model (DOM)


Window object
represents the JavaScript global object
    - all variables and functions declared globally with the var keyword become the properties of the window object
    alert()
    confirm()
    promt()
    setTimeout()
        - sets a timer and executes a callback function once the timer expires
    setInterval()
        - executes a callback function repeatedly with a fixed delay between each call

setTimeout()
basic syntax:

var timeoutID = setTimeout(callbackFunction{, delay, arg1, arg2, argn...});
delay is in milliseconds
delay will default to 0
 */

// function timeoutEx(){
//     setTimeout(function (){
//         // alert("Hello Charlie Cohort and welcome to CodeBound!")
//
//         window.location = "https://www.yahoo.com";
//     },
//         5000
//     );
// }
// timeoutEx();

/*
setInterval()
setInterval ( CallBackFunciton(), delay, arg1, arg2, ...)
 */

// function intervalEx(){
//     setInterval(function(){
//         alert("Hello World!")
//     },
//         3000
//     )
// }
// intervalEx();
// clearInterval(); // allows us to stop the setInterval();

// var count = 0
// var max = 10
// var inverval = 2000
//
// var intervalID = setInterval(function (){
//     // conditionals
//     if (count >= max){
//         clearInterval(intervalID);
//         console.log("Finished");
//     } else {
//         count++;
//         console.log(count);
//     }
// },
//     inverval);

/*
DOM - Document Object Model
manipulate HTML using JavaScript

Locating elements:
target them by :
    - element (tag) ex <h1>
    - Class .
    - ID #

getElementBy
basic syntax
document.getElementBy("name of element/class/ID")
 */

//target id = 'btn3'

// var btn3clicked = document.getElementById('btn3');
// alert(btn3clicked);

//accessing FORM inputs
//we can access forms using the form collection

// var usernameInput = document.forms.login.username;
// console.log(usernameInput);

//accessing html elements using class
// var cards = document.getElementsByClassName('card');
// console.log(cards);

//accessing elements using tag
// var sections = document.getElementsByTagName('section');
// console.log(sections);

//querySelector()
//returns the first element within that document tha matches the specified selector or group of selectors

// var headerTitle = document.querySelector('header h1');
// var mainTitle = document.querySelector('#main-title');
//
// var cardSelector = document.querySelectorAll('.card');

// console.log(headerTitle);
// console.log(mainTitle);
// console.log(cardSelector);

// Accessing/modyfying elements and properties

// get the value of innerHTML

// var title = document.getElementById('main-title');
// console.log(title); //get the structure of #main-title

//only want the content of the element?
// console.log(title.innerHTML);

//or
// console.log(document.getElementById('main-title').innerHTML);

//set the value of the innerHTML
// title.innerHTML = "Hello <em>Charlie Cohort</em>!";

// document.getElementById('main-title').innerHTML = '<h1>Hello World!</h1>';

// Accessing and modifying using attributes
//check if attribute exists

// var checkForm = document.forms['login'];

// console.log(checkForm);
// console.log(checkForm.hasAttribute('action'));

//get an attribute value
// console.log(checkForm.getAttribute("method"));

//crate a new attribute or change a value of an existing one
// checkForm.setAttribute('id','feedback-form');
//adds an ID name
// checkForm.setAttribute('method', 'GET');
//changes the method from POST to GET

// console.log(checkForm);

//delete attribute
// checkForm.removeAttribute('id');
// console.log(checkForm);

//accessing and modyfying styles

//single style
var jumbotron = document.querySelector('.jumbotron');
// jumbotron.style.display = 'none';
/*
same as in CSS

.jumbotron{
    display: none
}
 */

jumbotron.style.fontFamily = "Comic Sans ms";

//multiple styles

Object.assign(jumbotron.style, {
    border: "10px solid blue",
    fontFamily: "Trajan",
    textDecoration: "underline"
});

//stying node list

// var tableRows = document.getElementsByTagName("tr");
// for (var i = 0; i < tableRows.length; i++){
//     if (i % 2 === 0 && i !== 0) {
//         tableRows[i].style.background = "skyblue";
//     }
// }

//convert tr to an array
var tableRowArray = Array.from(tableRows);

//now we can use a forEach
tableRowArray.forEach(function (tableRow){
        tableRow.style.background = 'skyblue';
});


//adding and removing elements
// createElement()
// removeChild()
// appendChild()
// replaceChild()