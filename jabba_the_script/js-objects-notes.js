"use strict";

// Objects
/*
object is a grouping of data and functionality
data items inside of an object = properties
functions inside of an object = methods


custom objects
prototupes that allows existing object to be used as templates to create new objects

Object() keyword - the starting point to make custom objects
 */

// new object instance
// var car = new Object();
// console.log(typeof car);

//the use of 'new Object' calls the Object Constructor to build a new Instance of object

//object literal notation
//curly braces

// var car = {};
// alert(typeof car);

//setting up properties on a custom object
//dot notation

// car.make = 'Toyota';
//
// //array notation
// car['model'] = '4Runner';
//
// console.log(car);

//most common way to assign properties
// var car = {
//     make: "Toyota",
//     model: "4Runner",
//     color: "Hunter Green",
//     numberOfWheels: 4,
//     isItTotalled: false,
// };
// console.log(car);
// console.log(car.model);
//
// //adding on more properties
//
// // don;t do this
// //car["numberOfDoors] = 5;
//
// //instead
// car.numberOfDoors = 5;
// console.log(car);


//nested values

var cars = [
    {
        make: 'Toyota',
        model: '4Runner',
        color: 'Hunter Green',
        numberOfWheels: 5,
        features: ["Heated Seat", "Bluetooth Radio", "Automatic Doors"],
        alarm: function () {
            alert("sound the alarm");
        }
    },{
        make: 'Honda',
        model: 'Civic',
        color: 'Black',
        numberOfWheels: 4,
        features: ["Great Gas Mileage", "FM/AM Radio", "Still Runs"],
        alarm: function () {
            alert("no alarm sorry");
        },
        owner: {
            name: 'John Doe',
            age:35,
        }
    }, {
        make: 'Nissan',
        model: 'Sentra',
        color: 'Red',
        numberOfWheels: 4,
        features: ["Great Gas Mileage", "Heated Seat", "FM/AM Radio", "Still Runs", "Powered Windows", "GPS Navigation"],
        alarm: function () {
            alert("call the cops");
        },
        owner: {
            name: 'Jane Doe',
            age: 30,
            address: {
                street: '150 Alamo plaza',
                city: 'San Antonio',
                state: 'TX',
            },
                citySlogan: function (){
                    alert("Go spurs go");
                }
            }
        }

]

console.log(cars);

//get the honda object
console.log(cars[1]);

//get the model of the honda object only
console.log(cars[1].model);

//get the owner of the nissan sentra only
console.log(cars[2].owner);

//get the owner of the nissan city
console.log(cars[2].owner.address.city);

//get the second feature of the honda civic only
console.log(cars[1].features[1]);

//get the alarm function for the first object
cars[0].alarm();


// display the features of all cars
cars.forEach(function (car){
        car.features.forEach(function (feature){
            console.log(feature);
        }
        )
    }
    )

// 'this' keyword
/*
this - is simply a reference to the current object
-can also refer to a different object based on how a function is being called
 */

//ex.

var videoGame ={};
videoGame.name = "Halo CE";
videoGame.company = "Bungie";
videoGame.genre = "FPS";

//create a method on the vidoeGame object

videoGame.description =
    function (){
        console.log(`Video Game: ${this.name} \nCompany of Video Game: ${this.company} \nGenre of Video Game: ${this.genre}`);
    };