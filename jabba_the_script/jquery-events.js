"use strict";

//mouse events
/*
.click() - event handler to the "click"
.dblclick() - event handler to the double click
.hover() - executed when the mouse pointer enters and leaves the element(s)
 */

//mouse event in js
/*
var eventElement = document.getElementByCId('my-id');
eventElement.addEventListener('click', function(){
code
};
 */

//mouse event in jquery
/*
syntax:
$('selector').click(handler/function);
 */

// $("#charlie").click(function (){
//     alert("the h1 element with the id of 'charlie' was clicked!")
// });

// $('#charlie').dblclick(function (){
//     alert("h1 element with the id of charlie was double clicked!");
// });

// research ".dblclick()" vs ".ondblclick()"

// .hover()
//$("selector").hover( handlerIn, handlerOut)
$('#charlie').hover(
    // handlerIn anonymous function
    function (){
        $(this).css('background-color', 'skyblue');
    },
    // handlerOut anonymous function
    function (){
        $(this).css({'background-color': 'white', 'font-size' : '24px'});
    }
);


//keyboard events in jQuery
/*
Different types of keyboard events
    .keydown()
    .keypress()
    .keyup()
    .on()
    .off()
 */

// .keydown() - event handler to the "keydown" js event/triggers that  event on an element
// syntax: $("selector").keydown( handler/anonymous function)

// $('#my-form').keydown(function (){
//     alert("keypress detected");
// });

// .keypress()
/*
same as keydown, with one exception: modifier keys (shift, ctrl, esc.)
    - will trigger keydown() but not keypress()
 */

//.keyup();
$('#my-form').keyup(function (){
    alert("keyrelease detected");
});

// .on() .off()