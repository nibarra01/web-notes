"use strict";


//Arrays
//a data structure that holds an ordered list of items
//a variable that contains a list of values

/*
[] an empty array
['red']
['red', 'blue', 'white'] 3 elements
[4, 4.4, -14, 0] an array of numbers, 4 elements
['red', 4, true, -4]
*/

var array = ['black', 12, -144, false,];
console.log(array);
console.log(array.length);
console.log(array[0]);
console.log(array[4]);

//Iterating array
// traverse through elements

var charlie = ['josh', 'jose', 'nick', 'karen', 'stephen'];
console.log(charlie);

console.log("There are " + charlie.length + " members in the Charlie Cohort");

//loop through an array and cl the values
for (var i = 0; i < charlie.length; i++){
    //print out all the values for charlie individually
    console.log("The person at index " + i + " is " + charlie[i]);
}

// forEach loop
/*
syntax:
nameOfVariable.forEach(function(element, index, array){
run the code
})
 */

var ratings = [25, 34, 57, 62, 78, 89, 91];
ratings.forEach(function (rating){
    console.log(rating);
})

var pizza = ["cheese", "dough", "sauce", "pepperoni", "mushrooms"];
pizza.forEach(function (ingredients){
    console.log(ingredients);
})

// changing or manipulating arrays
var fruits = ['banana', 'apple', 'grape', 'strawberry'];
console.log(fruits);

//adding to the array
// .push = add to the end of the array

fruits.push("watermelon");
console.log(fruits);

//.unshift = add to the beginning of an array
fruits.unshift("dragon fruit");
console.log(fruits);
console.log(fruits[0]);

fruits.push("cherry", "mango", "pineapple");
console.log(fruits);

// remove from array
// .pop() = remove last element of an array

fruits.pop();
console.log(fruits);

//.shift() = remove the first element
fruits.shift();
console.log(fruits);

var removedFruit = fruits.shift();
console.log("Fruit Removed: " + removedFruit);


//locating array elements
console.log(fruits);

//.indexOf = returns the first occurrence of what you're looking for and the index
var indexElement = fruits.indexOf('watermelon');
console.log(indexElement);

//.slice = copies a portion of the array
//returns a new array (does not modify the original)

var slice = fruits.slice(2,5);
console.log(slice);
console.log(fruits);

// reverse
// .reverse = reverse the original array and returns the reversed array

fruits.reverse();
console.log(fruits);


//sort
// .sort = sort the original array and return the sorted array

console.log(fruits.sort());

//split
// .split = takes a string and turns it into an array

var codebound = 'leslie,stephen,karen'
var codeboundArray = codebound.split(',');
console.log(codeboundArray);

//join
//.join = takes an array and converts it to a string with a delimiter of your choice
var newCodebound = codeboundArray.join(", ");
console.log(newCodebound);