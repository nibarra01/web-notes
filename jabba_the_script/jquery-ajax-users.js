"use strict";
// load users in our console log
$.ajax("https://jsonplaceholder.typicode.com/users",{
    type: "GET",
}).done(function (data){
    console.log(data);

    var dataHTML = displayUsers(data);
    $("#users").html(dataHTML);
});

function displayUsers(users){
    var usersOnHTML ='';

    users.forEach(function (user){
        usersOnHTML += `
        <div class="user">
            <h3>Employee Name: ${user.name}</h3>
            <a href="http://${user.website}" target="_blank">professional website: ${user.website}</a>
            <ul>
                <li>
                    <p>Username: ${user.username}</p>
                </li>
                <li>
                    <p>Address ${user.address.street}, ${user.address.city}</p>
                </li>
            </ul>
        </div>
        `
    })
    return usersOnHTML;
}