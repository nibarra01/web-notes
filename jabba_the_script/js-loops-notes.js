"Use Strict";

//javascript loops
//allows us to execute code repeatedly

//while, do-while, for
//break and continue

//while loop
//basic looping construct that will execute a block of code, as long as a condition is true

/*
syntax:
while (condition) {
 //   code
}
 */

//while loop
var num = 0;
while (num <= 100){
    console.log('#' + num);
    //increment a value
    num += 3;
    // num ++;
}

//do-while loop
/*
the only difference from a while loop is that the condition is evaluated at the end of the loop instead of the beginning
 */

/*
syntax:
do {
//code
} while (condition)
 */

var i = 10;
do{
    console.log("do while #" + i);
    i--;
} while (i >= 0);


//for loop
/*
a robust looping mechanism available in many programming languages
 */

/*
syntax:
for(initialization; condition; increment/decrement){
//code
}
 */


 for (var x = 100; x <= 200; x+=10){
     console.log("for loop #" + x);
 }

 //break and continue
/*
-breaking out of a loop
-using the break keyword allows us to exit the loop
 */

var endAtNumber = 5;
for(var n = 1; n <= 100; n++){
    console.log("loop count #" + n);
    if (n === endAtNumber){
        alert("we have reached our limit. break.");
        break;
        console.log("doyouseethismessage");
    }
}

// continue
// continues the next iteration of a loop
for (var N = 1; N <= 100; N++){
    if (N % 2 !== 0){
        continue;
    }
    console.log("Even number: "+N);
}