"use strict";

// Event listeners
// addEventListener()

//basic syntax
//target.addEventListener( type, listener, useCapture );

// Target = the object the event listener is registered on
// Type = the type of event that is being listened for
// Listener = the function that is called when an event of type happens on the target
// UseCapture = a boolean that decides if the event-capturing is used for event triggering

//Type of events
// keyup - key is relased
// keydown - key is pressed down
// click - mouse is clicked
// change - input gets modified
// submit - when form is submitted
// mouseover

var testButton = document.getElementById('testBtn');

//add event listener using an anonymous function
// testButton.addEventListener('click', function (){
//     if (document.body.style.background === 'red'){
//         document.body.style.background = 'white';
//     } else {
//         document.body.style.background = 'red';
//     }
// });

//add event listener from a previously defined function
function toggleBackgroundColor(){
    if (document.body.style.background === 'red'){
        document.body.style.background = 'white';
        testButton.removeEventListener('click', toggleBackgroundColor);
    } else {
        document.body.style.background = 'red';
    }

}

testButton.addEventListener('click', toggleBackgroundColor);

// Register additional events

// cursor hovers over a paragraph, change the color of the text, font-family and make font larger

var paragraph = document.querySelector("p")

// var paragraph = document.getElementsByTagName("p")[0];

//change font color
function makeColorChange(){
    paragraph.style.color = "green";
    paragraph.style.fontFamily = "comic sans ms";
    paragraph.style.fontSize = "30px";

}

//add a mouse over event to the 'p' that will trigger the makeColorChange()

paragraph.addEventListener('mouseover', function (){
    makeColorChange();
});