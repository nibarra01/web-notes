"use strict";
//defines that js code should be executed in strict mode
//with strict mode you cannot use undeclared variables

//if statements
//execute code based on certain conditions

//syntax
// if (condition){
//    code executed if condition is true
// }


// var numberOfLives = 1;
// if (numberOfLives === 0){
//     alert("git gud");
// }


// if/else statement
//else statement executes when condition is false
// if (condition){
//    code executed if condition is true
// }
// else {
//    code executed if statement is fasle

//example
var b = 1;
if (b===10){
    console.log('b is 10');
}
else if (b<10) {
    console.log("b is less than 10");
}
else{
        console.log("b is NOT entered");
    }


//switch statement
//less duplicated and increases readability in code

//syntax

/*
switch (condition){
    case //code that gets executed
    break; //stops code when case is executed
    case''
    break;
    ..
    ..
    default '' // equivalent to the else statement
    break;
 */

// example
var color = "red";

switch (color){
    case "blue":
        console.log("you chose blue");
        break;
    case "red":
        console.log("you chose red");
        break;
    case "pink":
        console.log("you chose pink");
        break;
    case "green":
        console.log("you chose green");
        break;
    default:
        console.log("you didn't select the right color");
        break;
}

// using a switch statement create a program for the weather
// sunny, cloudy, rain, snow, default

var currentWeather = overcast;

switch(currentWeather){
    case "sunny":
        console.log("it's currently sunny");
        break;
    case "cloudy":
        console.log("it's currently cloudy");
        break;
    case "rain":
        console.log("it's currently raining");
        break;
    case "snow":
        console.log("it's currently snowy");
        break;
    default:
        console.log("not currently implemented");
        break;
}


//ternary operator
//shorthand way of creating if/else statements
//only used when there are two choices

//syntax
// (if condition is true) ? run this code : otherwise run this code

var numberOfLives   = 6;
(numberOfLives === 0) ? console.log('game over') : console.log("still alive");

