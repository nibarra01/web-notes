"use strict";
//promises
// a tool for handling asynchronous events
/* a promise represents an event that might not yet have happened
a promise will be in one of three states:
- pending - the event has not yet happened
- resolved - the event haas happened successfully
- rejected - the event has happened and an error condition occured
 */

// const myPromise = new Promise((resolve, reject) => {
//     if (Math.random() > 0.5){
//         resolve();
//     } else {
//         reject();
//     }
// });
// myPromise.then(() => console.log('Promise Resolved'));
// myPromise.catch(() => console.log('Promise rejected'));

// a promise object has two used methods
// .then() = accepts a callback that will run when the promise is resolved
//.catch() = accepts a callback that will run when the promise is rejected

//
// const mypromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if (Math.random() > 0.5) {
//             resolve();
//         }
//         else {
//             reject();
//         }
//     },5000)
//
// });
//
// console.log(mypromise)
//
// mypromise.then(() => console.log("promise resolved"));
// mypromise.catch(() => console.log("promise rejected"));


//write a promise inside of a function
// function makeARequest () {
//     return  new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (Math.random() > 0.5) {
//                 resolve("here is your data");
//             }
//             else {
//                 reject("network connection error");
//             }
//         },5000)
//
//     });
//
// }
// const request = makeARequest()
// console.log(request);
//
// request.then(data => console.log("promise resolved", data));
// request.catch(error => console.log("promise rejected", error));

//use a fetch and the promises returned from it

// const mypromise = fetch('https://api.github.com/users')
// mypromise.then(response => console.log(response));
// mypromise.catch(error => console.log(error));

// chaining
// - the return value from a promise's callback that can itself be treated as as promise which allows us to chain promises together

// const mypromise = new Promise()

Promise.resolve('one').then((one) => {
    console.log(one);
    return 'two';
}).then((two) => {
    console.log(two);
    return 'three';
}).then((three) => {
    console.log(three)
});


const mypromise =
    fetch ('https://api.github.com/users')
.then((response) => {
    const users =
        response.json().then((users) => {
            users.forEach((user) => {
                // do someething with each user object
                console.log(user);
            });
        });
});