"use strict";

/*
    for:
    basic syntax:

for (initialization; condition; increment/decrement){
    //run the code
}
 */

// for (var i = 100; i>=50; i--){
//     console.log(i);
// }

/*
    forEach
    basic syntax:

nameOfVariable.forEach(function (element, index, array){
//code
})
 */

/*
    syntax of a function:
function nameOfFunction(){

}
 */

var i = [100, 90, 80, 70];
i.forEach(function (i1){
    console.log(i1);
})