// Exponentation operator

//old way
// alert(Math.pow(2,8)); //256

//es6 way
// alert(2 ** 8) // 256

//const keyword
//  - blocked scope variable(s)
//  - cannot be changed

/*
if(true){
var cohort = 'charlie';
}

if(true){
const cohort = 'charlie';
}
 */

/*
var x = 10;
// Here x is 10
{
const x = 2
//here x is 2
}
console.log(x);
//here X is 10
 */

// FOR ... OF
/*
Syntax

for (const element of iterable ) {

}

EXAMPLE:
const x = [10,20,30];
for (const value of x){
console.log(value);
}
 */


//ARROW FUNCTIONS
/*
SYNTAX (js)
const sayHellp = function (cohort){
return 'Hello from ' + cohort;
};

SYNTAX (es6)
const sayHello = (cohort) => 'Hello from ' + cohort;
const sayHello = cohort => 'Hello from ' + cohort;


SYNTAX (js)
const x = function(x, y){
return x * y;
}

SYNTAX (es6)
const x = (x,y) => x * y;
 */


//  DEFAULT FUNCTION PARAMETER VALUES
//      ES6 allows function parameters to have default values
/*
SYNTAX (js)
function greeting(cohort){
    if (typeof cohort === 'undefined'){
    cohort = 'World';
    }
return console.log("Hello, " + cohort);
};

greeting(); //Hello, World
greeting('charlie'); //Hello, charlie

SYNTAX (es6)
const greeting = (cohort = 'World') => console.log(`Hello from ${cohort});

function greeting(cohort = 'World'){
return console.log)"Hello, " + cohort);
};
greeting(); //Hello, World
greeting('charlie') //Hello, charlie
 */

// Object property variable assignment shorthand

/*
Objects from JS

var person = {
    name: 'Charlie',
    age: 3
};

Objects from ES6
const name = 'Charlie'
const age = age
const person = {
    name,
    age,
};
 */


//Object destructuring
// shorthand for creating variables from object properties

/*
JS

var person = {name: 'Charlie', age:3}'
var name = person.name
var age = person.age

console.log(name); //Charlie
console.log(age); //3


ES6

const person = {name:'Charlie',age:3};
const {name,age} = person;
console.log(name) //Charlie
console.log(age) //3



ES6

function personInfo(person){
    var name = person.name;
    var age = person.age
    console.log(name);  //Charlie
    console.log(age);   //3
    };

    const person = {name'Charlie',age:3};
    personInfo(person)
 */



// Destructuring Arrays
// we can also use destructuring with arrays

/*
const myArray = [1,2,3,4,5];
const [x,y,thirdIndex,bob,karen] = myArray;
console.log(x);     //1
console.log(y);     //2
console.log(bob);   //4
 */