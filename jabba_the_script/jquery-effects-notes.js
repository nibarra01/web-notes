"use strict";

// jQuery effects
/*
    .hide() - hide the element(s)
    .show() - display the element(s)
    .toggle() - display/hide the element(s)
 */


//when you click on the text 'Individual Artists', hide the listing of individual artists
// $("selector").click(handler)

// $('#music-artist-toggle').click(function (){
//    $('#music-artist').hide();
// });

// .show()
// $('#music-artist-toggle').click(function (){
//     $('#music-artist').show();
// });


//toggle();
// $('#music-band-toggle').hover(function () {
//     $('#music-band').show();
// },
//     function (){
//     $("#music-band").hide();
//     });
//
// $('#music-artist-toggle').hover(function () {
//     $('#music-artist').toggle();
// });

//animated effects
// .fadeIn() .fadeOut() .fadeToggle()

$("#music-band-toggle").dblclick(
    function (){
        $("#music-band").fadeToggle(5000);
    }
);

// sliding effects
// .slideUp() .slideDown() .slideToggle()
$("#music-artist-toggle").click(
    function (){
        $("#music-artist").slideToggle(8000);
    }
);

$(document).hover(function (){
    $('#message').fadeIn(5000);
})