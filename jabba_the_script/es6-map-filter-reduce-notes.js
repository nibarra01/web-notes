"use strict";

/*
MAP, FILTER AND REDUCE

-   all functions that operate on collections (arrays)
-   all will modify but will return a new copy of the array

.map - transform each element in the collection
.filter - filters our values
.reduce - a collection to a single value
 */

// var numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];
// var evens = [];
// var odds = [];

// for (var i = 0; i < numbers.length; i++) {
//     // find all the even numbers
//     if (numbers[i] % 2 === 0){
//         // add the index from number to the evens
//         evens.push(numbers[i]);
//     }
// }
//
// for (var x = 0; x < numbers.length; x++) {
//     // find all the even numbers
//     if (numbers[x] % 2 !== 0){
//         // add the index from number to the evens
//         odds.push(numbers[x]);
//     }
// }
//
// console.log(evens);
// console.log(odds);



// var evens = numbers.filter(function (n){
//     return n % 2 ===0;
// });
//
// console.log(evens);
//
// var odds = numbers.filter(function (n){
//     return n % 2 !==0;
// });
//
// console.log(odds);
//
// console.log(numbers);
//
//
// // MAP
// var increment = numbers.map(function (num){
//     return num +2;
// });
//
// console.log(increment);

// examples above in es6
// const numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];
// const evens = numbers.filter(n => n % 2 === 0);
// console.log(evens);
//
// const odds = numbers.filter(num => num % 2 !== 0);
// console.log(odds);
//
// const increment = numbers.map(num3 => num3 + 2);
// console.log(increment);


// Reduce
/*
.reduce used to reduce a collection ot a single value
 */

// const nums = [1,2,3,4,5];
// const sum = nums.reduce((accumulation, currentNumber) => {
//     return accumulation + currentNumber;
// }, 0);
// console.log(sum);


const officeSales = [
    {
        name: 'Jim Halpert',
        sales: 500
    },
    {
        name: 'Dwight Schrute',
        sales: 750
    },
    {
        name: 'Ryan Howard',
        sales: 150
    }
];

const totalSales = officeSales.reduce((total, person) => {
    return total + person.sales;
},0);
console.log(totalSales);