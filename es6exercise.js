const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'php', 'java']
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'angular', 'spring boot']
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['java', 'aws', 'php']
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['javascript', 'java', 'sql']
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql']
    }
];

// ----------------- 1

/*
const x = 12;
console.log(x);
x = 14;
console.log("---");
console.log(x); //Uncaught TypeError: Assignment to constant variable at es6exercise.js:31
 */

// --------------------- 2


const name = "Nick";
const email = "nicholasibarra@gmail.com";
const languages = ["html","CSS",'Bootstrap', "JavaScript", 'jQuery', 'ES6'];

// console.log(developers);
// console.log("-----");

const addition = {name, email, languages};
developers.push(addition);
// console.log(developers);



// -------------------- 3

/*

let emails = [];
let names = [];
let languages [];

developers.forEach ( developer => emails.push(developer.email));
developers.forEach ( developer => names.push(developer.name));
developers.forEach ( developer => languages.push(developer.name));


//  for (const entry of developers){
//      emails.push(entry.email);
//      names.push(entry.name);
//     languages.push(entry.name);
//    }
console.log(emails);
console.log(names);
console.log(languages);

 */

const developerTeam = [];

for (const currentIndex of developers){
    developerTeam.push(`${currentIndex.name}'s email is ${currentIndex.email} \n${currentIndex.name} knows ${currentIndex.languages.join(', ')}`);
}

for (const log of developerTeam){
    console.log(log);
}

// -------------------- 4

let list = '<ul>';

for (const ul of developerTeam){
    console.log(ul)

    list += `<li> ${ul} </li>`;
}
list += '</ul>'
document.write(list)
