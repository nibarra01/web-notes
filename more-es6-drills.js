const petName = 'Chula';
const age = 14;
// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + petName +
//     ' and she is ' + age + ' years old.');
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };
// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }

console.log(`My dog is named ${petName} and she is ${age} years old.`);

const add10 = num1 => num1 + 10;

const minus5 = num1 => num1 - 5;

const multiplyByTwo = num1 => num1 * 2;

const greetings = name => `Hello, ${name} how are you?`;

const haveWeMet = name => {if (name === 'Bob'){
    // console.log(name);
    return `${name} Nice to see you again`
} else {
    // console.log(name);
    return "Nice to meet you"
}}

/*
const haveWeMet = name => (name === 'Bob') ? "Nice to see you again" : "Nice to meet you";
TERNARY OPERRATOR
SYNTAX
condition ? message if condition is true : message if condition is false
 */

console.log(`4 plus 10 is ${add10(4)}`);
console.log(`4 minus 5 is ${minus5(4)}`);
console.log(`4 times 2 is ${multiplyByTwo(4)}`);
console.log(greetings("The developer formerly known as Prince"));
console.log(haveWeMet('Bob'));
console.log(haveWeMet("Super Sayian Bob"));

