"use strict";

function task1() {
    var input = prompt("insert a number", "4");
    var num = parseInt(input);
    if (isNaN(num))
    {
        alert("not a number");
    }
    else{
        alert(`${num} + 100 is ${num+100}`);
        if(num % 2 === 0){
            alert(`additionally, ${num} is even`);
        }
        else{
            alert(`additionally, ${num} is odd`);
        }
        // switch (num){
        //     case > 0:
        //         alert(`and ${num} is positive`);
        //         break;
        //     case < 0:
        //         alert(`and ${num} is negative`);
        //     case === 0:
        //         alert(`zero isn't positive or negative`);
        if(num === 0){
            alert("zero is neither negative or positive");
        }
        else if(num>0){
            alert(`${num} is positive`);
        }
        else {
            alert(`${num} is negative`);
        }
    }

}

var question = confirm("do you want to enter a number");
    if (question===true){
        task1();
    }
    else{
        alert("k bye");
    }


//    -----------------

function calculateTotal(ticket,total){
        var Ticket = parseInt(ticket);
        var Total = parseInt(total);

        if (Ticket < 0 || Ticket > 5 || isNaN(Ticket)){
            alert("invalid ticket");
            // console.log("after ticket check");
        }else if (Total < 0 || isNaN(Total)){
            alert("invalid total");
            // console.log("after total check");
        }
        else{
            // console.log("start of switch");

        switch (Ticket){
            case 0:
                alert(`total with ticket ${Ticket} is ${Total}`);
                // console.log("ticket check 0");
                break;
            case 1:
                alert(`total with ticket ${Ticket} is ${Total*.9}`);
                // console.log("ticket check 1");
                break;
            case 2:
                alert(`total with ticket ${Ticket} is ${Total*.75}`);
                // console.log("ticket check 2");
                break;
            case 3:
                alert(`total with ticket ${Ticket} is ${Total*.65}`);
                // console.log("ticket check 3");
                break;
            case 4:
                alert(`total with ticket ${Ticket} is ${Total*.5}`);
                // console.log("ticket check 4");
                break;
            case 5:
                alert(`total with ticket ${Ticket} is ${Total*.25}`);
                // console.log("ticket check 5");
                break;
            default:
                alert("something happened");
                console.log("something happened");
                break;
        }
        }

}

    var ticket = prompt("input lucky ticket number");
    var total = prompt("input total");

// forgot to actually call the function
calculateTotal(ticket,total);



// -----------------------------------------

function numberGradeToLetterGrade (grade){
    if (grade < 0 || isNaN(grade)===true){
        alert("invalid grade");
    }else if (grade > 100){
        alert("grade over 100, extra credit points awarded?");
    }else{
            switch (grade){
                case "100":
                case "99":
                case "98":
                case "97":
                alert("A+");
                break;

                case "96":
                case "95":
                case "94":
                alert("A");
                break;

                case "93":
                case "92":
                case "91":
                case "90":
                alert("A-");
                break;

                case "89":
                case "88":
                case "87":
                alert("the worst grade imaginable, an A minus MINUS");
                break;

                case "86":
                case "85":
                case "84":
                alert("B");
                break;

                case "83":
                case "82":
                case "81":
                case "80":
                alert("B-");
                break;

                case "79":
                case "78":
                case "77":
                alert("C+");
                break;

                case "76":
                case "75":
                case "74":
                alert("C");
                break;

                case "73":
                case "72":
                case "71":
                case "70":
                alert("C-");
                break;

                case "69":
                case "68":
                case "67":
                alert("D+");
                break;

                case "66":
                case "65":
                case "64":
                alert("D");
                break;

                case "63":
                case "62":
                case "61":
                case "60":
                alert("D-");
                break;

            default:
                alert("F");
                break;
        }
    }
}

var grade = prompt("input grade");
numberGradeToLetterGrade(grade);

// let test = "potato";
// console.log(parseInt(test));
// console.log(isNaN(test));


// ------------------------------------------

function greetingForMilitaryTimes(time){
    if (time<0 || time > 25){
        alert("invalid input");
    } else if (time <= 11 && time >= 6){
        alert("Good morning.");
    } else if (time >= 12 && time <= 17) {
        alert("Good afternoon.")
    } else if (time >= 18 && time <= 22){
        alert("Good evening.");
    } else if (time >= 23 || time <= 24 || time <= 5) {
        alert("Good night");
    }
    else {
        alert("Input not accepted.");
    }
}

var time = prompt("Input current time (hour only) (24h format)");
greetingForMilitaryTimes(time);


// ---------------------------------------

function color(input){
    if (input === "blue"){
        alert("Blue is the color of the sky (most of the time)");
    } else if (input === "red"){
        alert("roses are usually red");
    } else {
        alert("there is no message available for this");
    }
}
var colour = prompt("Type in a color of your choice");
color(colour);


// --------------------------------------------------

var month = prompt("input current month");

function seasons(month){
    switch (month){
        case "December":
        case "Dec":
        case "12":
        case "december":
        case "dec":
        case "DECEMBER":
        case "DEC":
        case "January":
        case "Jan":
        case "1":
        case "01":
        case "january":
        case "jan":
        case "JANUARY":
        case "JAN":
        case "February":
        case "Feb":
        case "february":
        case "feb":
        case "FEBRUARY":
        case "FEB":
        case "2":
        case "02":
            alert("the current season is Winter");
            break;

        case "March":
        case "Mar":
        case "march":
        case "mar":
        case "MARCH":
        case "MAR":
        case "3":
        case "03":
        case "April":
        case "Apr":
        case "april":
        case "apr":
        case "APRIL":
        case "APR":
        case "4":
        case "04":
        case "May":
        case "may":
        case "MAY":
        case "5":
        case "05":
            alert("the current season is Spring");
            break;

        case "June":
        case "Jun":
        case "june":
        case "jun":
        case "JUNE":
        case "JUN":
        case "06":
        case "6":
        case "July":
        case "Jul":
        case "july":
        case "jul":
        case "JULY":
        case "JUL":
        case "07":
        case "7":
        case "August":
        case "Aug":
        case "august":
        case "aug":
        case "AUGUST":
        case "AUG":
        case "08":
        case "8":
            alert("the current season is Summer");
            break;

        case "September":
        case "Sep":
        case "september":
        case "sep":
        case "SEPTEMBER":
        case "SEP":
        case "09":
        case "9":
        case "October":
        case "Oct":
        case "october":
        case "oct":
        case "OCTOBER":
        case "OCT":
        case "10":
        case "November":
        case "Nov":
        case "november":
        case "nov":
        case "NOVEMBER":
        case "NOV":
        case "11":
            alert("the current season is Fall");
            break;

        default:
            alert("string not a month, or not in a recognized format.");
    }
}

//forgot I have go actually call the funciton again
seasons(month);

// --------------------------------------------------------


function isThisANumber(){
    var mystery = prompt("Alright gang, let's split up and input a string", "zoinks");
    var solved = parseInt(mystery);
    (isNaN(solved) === true) ? alert("No integers detected") : alert("Number detected");
}

isThisANumber();

//---------------------------------------------------------------


function favoriteDay(){
    var day = prompt("what is your favorite day?");
    switch (day) {
        case "Monday":
        case "Mon":
        case "M":
        case "m":
        case "monday":
        case "mon":
            alert("but why?");
            break;

        case "Tuesday":
        case "Tue":
        case "tuesday":
        case "tue":
        case "Tu":
        case "tu":
            alert("a fan of tacos perhaps?");
            break;

        case "Wednesday":
        case "wednesday":
        case "Wed":
        case "wed":
        case "W":
        case "w":
            alert("never really was a fan of the term 'hump day'. No calender has the days of the week as a curve, or any sort of arrangement where Wednesday is at a higher elevation than the other days.");
            break;

        case "Thursday":
        case "thursday":
        case "Thu":
        case "thu":
        case "Thurs":
        case "thurs":
        case "Th":
        case "th":
            alert("Welcome to the unique message for Thursday.");
            break;

        case "Friday":
        case "friday":
        case "Fri":
        case "fri":
        case "F":
        case "f":
            alert("The start of the weekend, unless you don't work monday-friday");
            break;

        case "Saturday":
        case "saturday":
        case "Sat":
        case "sat":
        case "Sa":
        case "sa":
            alert("The worst day to get stuff done since everyone is out getting stuff done they couldn't take care of during the week");
            break;

        case "Sunday":
        case "sunday":
        case "Sun":
        case "sun":
        case "Su":
        case "su":
            alert("I wouldn't pick Sunday. Both Chick-fil-a and Las Pallapas are closed.");
            break;

        default:
            alert("You didn't enter a day, or entered a day in an unrecognized format");
            break;
    }
}

favoriteDay();

//-----------------------------------------------------

