"use strict";

// -----------------------------

var eiffel65 = document.getElementById('change-bg-color');
function blueDaBaDee(){
    if (document.body.style.background === 'blue'){
        document.body.style.background = 'none';
    } else if (document.body.style.background !== 'blue') {
        document.body.style.background = 'blue';
    }
}
eiffel65.addEventListener('click', blueDaBaDee);

// -----------------------------------

var appendages = document.getElementById("append-to-ul");
var appendable = document.getElementById("append-to-me");


function newText(larry){
    var li = document.createElement("li");
    li.textContent = larry;
    return li;
}

appendages.addEventListener('click', function (){
        appendable.appendChild(newText("text"));
});

// ---------------------------------------------------------

(function(){
    setInterval(function (){
        document.querySelector("#message").innerHTML = "Goodbye World!"
    }, 2000);
})();


// --------------------------------------------------

var yeller = document.getElementById("hl-toggle");
// console.log(yeller.children.length);
// console.log(yeller.children.item(0));
// yeller.children.item(0).setAttribute("style","background : yellow")

// yeller.addEventListener('click',function (){
    // if (yeller.style.background !== "yellow"){
    //     yeller.style.background = "yellow";
    // } else if (yeller.style.background === "yellow"){
    //     yeller.style.background = "none";
    // }


    // yeller.childNodes.forEach(function (n){
    //     if (yeller.children.item(n).style.background !== "yellow"){
    //         yeller.children.item(n).style.background = "yellow";
    //         console.log(`to yellow @ ${n}`);
    //     } else if (yeller.children.item(n).style.background === "yellow"){
    //         yeller.children.item(n).style.background = "none";
    //         console.log(`from yellow @ ${n}`);
    //     }
    // })

    // yeller.childNodes.forEach(function (x){
    //     if (yeller.children.item(x) === "li"){
    //         yeller.children.item(x) = "li style='background: yellow'";
    //     } else if {
    //         if (yeller.children.item(x) !== "li"){
    //         yeller.children.item(x) = "li";
    //     }
    // })

// });

for (var x = 0; x < yeller.children.length; x++){
    yeller.children.item(x).addEventListener('click',function (){
        // if (this.getAttribute('style') !== 'background : yellow'){
        //     this.setAttribute('style','background : yellow');
        // } else {
        //     this.setAttribute('style', 'background : none');
        // }
        // console.log(this.textContent);
        // this.innerHTML = '<li style="background : yellow"> test </li>'


        var twoAM = this.textContent
        // if (this.getAttribute('style') !== 'background : yellow'){
        if (this.innerHTML !== `<li style="background : yellow">${twoAM}</li>`){
            this.innerHTML = `<li style="background : yellow">${twoAM}</li>`;
        } else {
            this.innerHTML = `<li style="background : none">${twoAM}</li>`;
        }

    })
}
// yeller.children.item(0).setAttribute('style','background : yellow')
// console.log(yeller.children.item(0).getAttribute('style'));

// -------------------------------------------------
var theButton = document.getElementById('upcase-name');
// var theInput = document.getElementById('input').value;
var theOutput = document.getElementById('output');



function upper(theName){
    toString(theName);
    return theName.toUpperCase();
}

theButton.addEventListener('click',function (){
    var theInput = document.getElementById('input').value;
    theOutput.innerText = "Your name uppercased is: " + upper(theInput);


    // console.log("****");
    // console.log("theInput.innerHTML");
    // console.log(theInput.innerHTML);
    //
    // console.log("theInput.innerText");
    // console.log(theInput.innerText);
    //
    // console.log("theInput");
    // console.log(theInput);
    //
    // console.log("theInput.textContent");
    // console.log(theInput.textContent);
    //
    // console.log("theInput.value");
    // console.log(theInput.value);
    //
    // console.log("theOutput");
    // console.log(theOutput);
    //
    // console.log("theOutput.innerText");
    // console.log(theOutput.innerText);
    // // console.log("****");
});


/*
var outputText = document.getElementById('output');
var inputText = document.getElementById('input');
var upperCaseBtn = document.getElementById('upcase-name');

function convertInput(userInput){
    var userInputUpcase = inputText.value.toUpperCase();
    // console.log(userInputUpcase);
    outputText.innerHTML = 'your name uppercased is: ' +userInputUpcase;
}

upperCaseBtn.addEventListener('click', function (){
    convertInput();
})
 */

/*
var outputText = document.getElementById('output');
var inputText = document.getElementById('input');
var upperCaseBtn = document.getElementById('upcase-name');
var upperName = inputText.value.toUpperCase();

upperCaseBtn.addEventListener('click', function convertInput(){
    var userInputUpcase = input.value.toUpperCase();
    console.log(userInputUpcase);
    outputText.innerHTML = 'Your name uppercased is: ' + userInputUpcase;
});
 */

//-----------------------------------

var ritaRepulsa = document.getElementById('font-grow');
var makeMyMonsterGrow = ritaRepulsa.children;

// makeMyMonsterGrow.item(0).setAttribute('style','font-size : 12');
// console.log(makeMyMonsterGrow.item(0).getAttribute('style'));
// console.log(makeMyMonsterGrow.item(0).innerHTML);
// console.log(makeMyMonsterGrow.item(0));






for (var i = 0; i < ritaRepulsa.children.length; i++) {
    makeMyMonsterGrow.item(i).addEventListener('dblclick', function (){
        if (this.getAttribute('style') == null){
            this.setAttribute('style','font-size : 12');
        } else {

            var greenRanger = this.getAttribute('style');
            var whiteRanger = greenRanger.length - 12;
            var morphinTime = parseInt(greenRanger.slice(greenRanger.length - whiteRanger, greenRanger.length));
            morphinTime *= 2;

            this.setAttribute('style', `font-size : ${morphinTime}px`);
             // this = `<li style="font-size : ${morphinTime * 2}">${lordZed}</li>`;


            // *******
            // console.log("this");
            // console.log(this);
            //
            // console.log("this.innerHTML");
            // console.log(this.innerHTML);
            //
            // console.log("greenRanger.length");
            // console.log(greenRanger.length);
            //
            // console.log("greenRanger");
            // console.log(greenRanger);
            //
            // console.log("whiteRanger");
            // console.log(whiteRanger);
            //
            // console.log("morphinTime");
            // console.log(morphinTime);
            //
            // console.log(this.getAttribute('style'))

            //*******
        }
    })
}
