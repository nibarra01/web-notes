(function () {
    "use strict";


    // ------------------------------------------
    function promiseOneFunction (time){
        return new Promise((resolve, reject) => {
            setTimeout(() =>{
                resolve(`You will see this after ${time/1000} seconds`);
            }, time );
        });
    }

    const promiseOne = promiseOneFunction(3500);
    console.log(promiseOne)
    promiseOne.then(a => console.log("", a));


    const wait = (delay) => {
        return new Promise(((resolve, reject) => {
            setTimeout(()=>{
                resolve(`you will see this after ${delay/1000} seconds`)
         }, delay);
      }))
    };
    wait(4000).then((message) => console.log(message));

    const joseWait = (time) => {
        return new Promise(((a, b) => {
            setTimeout(() => {
                a(`You will see this after ${time/1000} seconds.`)
            }, time);
        }))
    }
    joseWait(6000).then(data => console.log('', data));



    // ----------------------------------------------------

    //------------------------------------------------
    function promiseTwo(compareTen){
        return new Promise( (resolved,rejected) => {
            if (compareTen < 10 || compareTen > 10){
                resolved("Yes, the value is less than or greater than 10");
            } else {
                rejected("No, the value is neither greater than nor less than 10");
            }
        })
    }

    const ten = promiseTwo(10.1);
    ten.then(a => console.log(a));
    ten.catch(a => console.log(a));


    const i = 5
    const testNum = new Promise((c, d) => {
        if (i < 10) {
            c('Value is less than 10');
        }
        else {
            d('Value is greater than 10');
        }
    });
    testNum.then(data => console.log('', data));
    testNum.catch(error => console.log('', error));



    // ------------------------------------------------

    //------------------------------------------------
    // function makeAllCaps(input){
    //     return input.toUpperCase();
    // }
    // function sortWords(input){
    //     return input.sort;
    // }

    // let promiseThreeString = prompt("input string of several words");
    // if (!isNaN(promiseThreeString)){
    //     console.log(isNaN(promiseThreeString))
    //     console.log(parseInt(promiseThreeString));
    //     alert("not a string. Everything will break now");
    // }else {
    //     let promiseThreeStringTwo = promiseThreeString.split(' ');
    //     // let promiseThreeStringThree = promiseThreeStringTwo.split(',');
    //     makeAllCaps(promiseThreeStringTwo).then(a => {
    //         sortWords(a);
    //     })
    // }

    const myArray = ['apples', 'tomatoes', 'cucumber', 'bananas'];
    const makeAllCaps = array => {
        return new Promise(((resolve, reject) => {
            var arrayCaps = array.map(word => {
                if (typeof word ==='string'){
                    return word.toUpperCase();
                }
                else {
                    reject('Error: not all items in the array are strings');
                }
            });
            resolve(arrayCaps);
        }));
    };
    const sortWords = array => {
        return new Promise(((resolve, reject) => {
            if (array){
                array.forEach((ele)=>{
                    if (typeof ele !== 'string'){
                        reject('something happened');
                    }
                });
                resolve(array.sort())
            }
            else {
                reject('something happened.')
            }
            }
           ));
    };

    makeAllCaps(myArray).then(sortWords).then((result) => console.log(result))
        .catch((error) => console.log(error));




})()