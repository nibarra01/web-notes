"use strict";


// **************************************************************
// -------------------------------------------
var task1Counter = 2;
function task1(){
    while (task1Counter <= 65537){
        console.log(`*   ${task1Counter}`);
        task1Counter *= 2;
    }
}

// ---------------------------------------------
function showMultiplicationTable(){
    var num = prompt("press button, receive multiplication table", "10");
    var intnum = parseInt(num);
    while (isNaN(intnum)===true){
        var num = prompt("But thou must", "10");
        var intnum = parseInt(num);
    }

    for (var x = 1; x <= num; x++ ){
        console.log(`${num} * ${x} = ${num * x}`)
    }
}
//-------------------------------------------------

function task3(){
    for(var x = 100; x >= 4; x -= 5){
        console.log(x);
    }

}
// ---------------------------------------------------

function task4(){
    var num = prompt("input an odd number between 1 and 50");
    var intnum = parseInt(num);

    while (1===1) {
        if (isNaN(intnum) === true) {
            var num = prompt("No.", "27");
            var intnum = parseInt(num);
        } else {
            break;
        }
    }

        while (1===1){
            if(intnum % 2 === 0){
                var num = prompt("Number is even.", "27");
                var intnum = parseInt(num);
            }else {
                break;
            }
        }

        while (1===1){
            if(intnum < 1 || intnum > 50){
                var num = prompt("Number is not in allowed range.", "27");
                var intnum = parseInt(num);
            }else {
                break;
            }
        }

        console.log(`Number to skip is ${intnum}`);
        for(var x = 1; x <= 50; x++){
            if (x % 2 === 0){
                continue;
            }

            if (x !== intnum){
                console.log(`Here is an odd number ${x}`);
            }else {
                console.log(`Yikes! Skipping number: ${x}`);
            }

        }


}

// -------------------------------------------------------------

function task5(){
    for(var x = 0; x < 21; x++){
        if (x % 2 === 0){
            console.log(`${x} is even.`);
        } else {
            console.log(`${x} is odd.`);
        }
    }
}
// ----------------------------------------------------------

function  task6(){
    for( var x = 0; x <11; x++){
        console.log(`${x} * 9 = ${x*9}`);
    }
}

//------------------------------------------------------------------

function task7(){
    var num = prompt("input an odd number between 1 and 50, again");
    var intnum = parseInt(num);

    while (1===1) {
        if (isNaN(intnum) === true) {
            var num = prompt("NO.", "27");
            var intnum = parseInt(num);
        } else {
            break;
        }
    }

    while (1===1){
        if(intnum % 2 === 0){
            var num = prompt("Number is even. Don't do that.", "27");
            var intnum = parseInt(num);
        }else {
            break;
        }
    }

    while (1===1){
        if(intnum < 1 || intnum > 50){
            var num = prompt("Number is not in allowed range.", "27");
            var intnum = parseInt(num);
        }else {
            break;
        }
    }

    console.log(`Number to skip is ${intnum}`);
    for(var x = 1; x <= 50; x++){
        if (x % 2 === 0){
            continue;
        }

        if (x !== intnum){
            console.log(`Here is an odd number ${x}`);
        }else {
            continue;
        }

    }


}

// -------------------------------------------------------

function taskBonus(){
    var numbers = prompt("input The Number");
    var numbas = parseInt(numbers);

    while (isNaN(numbas)===true){
        var numbers = prompt("This is not a The Number");
        var numbas = parseInt(numbers);
    }

    while (numbas <= 0){
        alert("The Negative will be removed from The Number");
        numbas = Math.abs(numbas);
        alert(`The Number is now ${numbas}`);
    }

    var numbarray = [];
    var numero = 0;
    for (var x = 1; x <= numbas; x++){
        var numero = numero += x;
        numbarray.push(x);
    }

    console.log(numero);
    console.log(numbarray.join(' + '));


}
// ***************************************************************

console.log("task 1");
task1();
console.log("");

console.log("task 2");
showMultiplicationTable();
console.log("");

console.log("task 3");
task3();
console.log("");

console.log("task 4");
task4();
console.log("");

console.log("task 5");
task5();
console.log("");

console.log("task 6");
task6();
console.log("");

console.log("task 7");
task7();
console.log("");

console.log("task Bonus");
taskBonus();
console.log("");



